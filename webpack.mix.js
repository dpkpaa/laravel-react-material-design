const path = require('path');
const mix = require('laravel-mix');

mix.react('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css',   {
        includePaths: [path.resolve(__dirname, 'node_modules')],
    })
    .options({ autoprefixer: false })
    .sourceMaps()
    // .browserSync('laravel-react.test');