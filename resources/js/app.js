import React from 'react';
import ReactDOM from "react-dom";
require('./bootstrap');
import Example from './components/Example';//Import Example Component
if (document.getElementById('app')) {//Create Base Instance of ReactDOM
    ReactDOM.render(<Example />, document.getElementById('app'));
}

import * as mdc from 'material-components-web';//Import All MDC JS
window.mdc = mdc;// Set MDC globally
window.mdc.autoInit();
new mdc.topAppBar.MDCTopAppBar(document.querySelector('.mdc-top-app-bar'));
new mdc.drawer.MDCDrawer(document.querySelector('.mdc-drawer'));

const floatingButtons = document.querySelectorAll('.mdc-fab');//Floating Buttons
floatingButtons.forEach( ele => {
    new mdc.ripple.MDCRipple.attachTo(ele);
});
const iconButtons = document.querySelectorAll('.mdc-icon-button');//Icon Buttons
iconButtons.forEach( ele => {
    const iconRipple =  new mdc.ripple.MDCRipple.attachTo(ele);
    iconRipple.unbounded = true;
});
const buttons = document.querySelectorAll('.mdc-button, .mdc-list-item');
buttons.forEach( ele => {
    new mdc.ripple.MDCRipple.attachTo(ele);
});