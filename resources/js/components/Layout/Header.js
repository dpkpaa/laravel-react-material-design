import React, {Component, Fragment} from 'react';
import Content from './Content';
export default class Header extends Component{
    constructor(props) {
        super(props);
        this.state = {isOpen: false};
        this.isOpenHandler = this.isOpenHandler.bind(this);
    };
    isOpenHandler(){
        this.setState({
            isOpen: !this.state.isOpen
        })
    };
    render(){
        return(
            <Fragment>
                <aside className={"mdc-drawer mdc-drawer--dismissible" + " " +(this.state.isOpen ? 'mdc-drawer--open' : '')}>
                    <div className="mdc-drawer__header"><h3 className="mdc-drawer__title">Mail</h3><h6
                        className="mdc-drawer__subtitle">email@material.io</h6></div>
                    <div className="mdc-drawer__content">
                        <nav className="mdc-list"><a className="mdc-list-item mdc-list-item--activated" href="#"
                                                     tabIndex="-1" aria-selected="true"><i
                            className="material-icons mdc-list-item__graphic" aria-hidden="true">inbox</i>Inbox</a><a
                            className="mdc-list-item" href="#" tabIndex="-1"><i
                            className="material-icons mdc-list-item__graphic" aria-hidden="true">star</i>Star</a><a
                            className="mdc-list-item" href="#" tabIndex="-1"><i
                            className="material-icons mdc-list-item__graphic" aria-hidden="true">send</i>Sent Mail</a><a
                            className="mdc-list-item" href="#" tabIndex="-1"><i
                            className="material-icons mdc-list-item__graphic" aria-hidden="true">drafts</i>Drafts</a>
                            <hr className="mdc-list-divider"/><h6 className="mdc-list-group__subheader">Labels</h6><a
                                className="mdc-list-item" href="#" tabIndex="-1"><i
                                className="material-icons mdc-list-item__graphic" aria-hidden="true">bookmark</i>Family</a><a
                                className="mdc-list-item" href="#" tabIndex="-1"><i
                                className="material-icons mdc-list-item__graphic" aria-hidden="true">bookmark</i>Friends</a><a
                                className="mdc-list-item" href="#" tabIndex="-1"><i
                                className="material-icons mdc-list-item__graphic"
                                aria-hidden="true">bookmark</i>Work</a>
                                <hr className="mdc-list-divider"/><a className="mdc-list-item" href="#" tabIndex="-1"><i
                                    className="material-icons mdc-list-item__graphic" aria-hidden="true">settings</i>Settings</a><a
                                    className="mdc-list-item" href="#" tabIndex="-1"><i
                                    className="material-icons mdc-list-item__graphic"
                                    aria-hidden="true">announcement</i>Help &amp; feedback</a>
                        </nav>
                    </div>
                </aside>
                <div className="mdc-drawer-app-content">
                    <header className="mdc-top-app-bar drawer-top-app-bar">
                        <div className="mdc-top-app-bar__row">
                            <section className="mdc-top-app-bar__section mdc-top-app-bar__section--align-start">
                                <button onClick={(this.isOpenHandler)} className="material-icons mdc-top-app-bar__navigation-icon mdc-ripple-upgraded--unbounded mdc-ripple-upgraded">menu
                                </button>
                                <span className="mdc-top-app-bar__title">Dismissible Drawer</span>
                            </section>
                            <section className="mdc-top-app-bar__section mdc-top-app-bar__section--align-end"
                                     role="toolbar">
                                <a href="#" className="material-icons mdc-top-app-bar__action-item"
                                   aria-label="Download">file_download</a>
                                <a href="#" className="material-icons mdc-top-app-bar__action-item"
                                   aria-label="Print this page">print</a>
                                <a href="#" className="material-icons mdc-top-app-bar__action-item"
                                   aria-label="Bookmark this page">bookmark</a>
                            </section>
                        </div>
                    </header>
                    <Content/>
                </div>
            </Fragment>
        );
    }
}