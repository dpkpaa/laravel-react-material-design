import React, {Component, Fragment} from 'react';
import Header from './Layout/Header';
export default class Example extends Component {
    render() {
        return (
            <Fragment>
                <Header/>
                    <h1 className="mdc-typography mdc-typography--headline5 text-center">Material Design Button</h1>
                    <div className="mdc-layout-grid">
                        <div className="mdc-layout-grid__inner">
                            <div className="mdc-layout-grid__cell">
                                <button className="mdc-button">Simple Button</button>
                            </div>
                            <div className="mdc-layout-grid__cell">
                                <button className="mdc-button mdc-button--raised">Raised Button</button>
                            </div>
                            <div className="mdc-layout-grid__cell">
                                <button className="mdc-button mdc-button--outlined">Outlined Button</button>
                            </div>
                            <div className="mdc-layout-grid__cell">
                                <button className="mdc-button mdc-button--unelevated">Unelevated Button</button>
                            </div>

                            <div className="mdc-layout-grid__cell">
                                <button className="mdc-button mdc-button--dense">Dense Button</button>
                            </div>

                            <div className="mdc-layout-grid__cell">
                                <button className="mdc-button mdc-button--outlined">
                                    <i className="material-icons mdc-button__icon">cloud_download</i>
                                    Icon Button
                                </button>
                            </div>
                        </div>
                    </div>


                    {/*Floating Button*/}
                    <h1 className="mdc-typography mdc-typography--headline5 text-center">Floating Buttons</h1>
                <div className="mdc-layout-grid">
                    <div className="mdc-layout-grid__inner">
                        <div className="mdc-layout-grid__cell">
                            <button className="mdc-fab" aria-label="Favorite">
                                <span className="mdc-fab__icon material-icons">favorite</span>
                            </button>
                        </div>
                        <div className="mdc-layout-grid__cell">
                            <button className="mdc-fab mdc-fab--extended">
                                <span className="material-icons mdc-fab__icon">add</span>
                                <span className="mdc-fab__label">Create</span>
                            </button>
                        </div>
                        <div className="mdc-layout-grid__cell">
                            <h1 className="mdc-typography mdc-typography--body1">Icon Button</h1>
                            <button className="mdc-icon-button material-icons">favorite</button>
                        </div>
                    </div>
                </div>


            </Fragment>
        );
    }
}
